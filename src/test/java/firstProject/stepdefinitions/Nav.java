package firstProject.stepdefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Nav {

    public static WebDriver driver;

    public enum ENavigateur {
        firefox,
        chrome,
    }

    public static WebDriver choisirNavigateur(ENavigateur nav) {
        switch(nav) {
            case firefox:
                System.setProperty("webdriver.gecko.driver", "src/main/resources/driver/geckodriver.exe") ;
                driver = new FirefoxDriver() ;
                return driver;
            case chrome:
                System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/chromedriver.exe");
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--remote-allow-origins=*");
               driver = new ChromeDriver(options);
                return driver;

            default: return null;

        }
    }
    public static void fermerNavigateur(WebDriver driver) {
        driver.close();
    }
}
