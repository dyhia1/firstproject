package firstProject.stepdefinitions;

import PageObject.PageAcceuil;
import PageObject.PageLogin;
import PageObject.PageWelcome;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class stepdefinitions {
  WebDriver driver;

    @Given("je suis sur la page login")
    public void je_suis_sur_la_page_login() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.findElement(By.xpath("//div[@id=\"MenuContent\"]/a[2]")).click();

    }
    @When("je saisis mon identifiant {string}")
    public void je_saisis_mon_identifiant(String username) {
        driver.findElement(By.xpath("//input[@name=\"username\"]")).clear();
       driver.findElement(By.xpath("//input[@name=\"username\"]")).sendKeys(username);
    }
    @And("je saisis mon mot de passe {string}")
    public void je_saisis_mon_mot_de_passe(String password) {
       driver.findElement(By.xpath("//input[@name=\"password\"]")).clear();
        driver.findElement(By.xpath("//input[@name=\"password\"]")).sendKeys(password);
    }
    @And("je clique sur Login")
    public void je_clique_sur_login() {
        driver.findElement(By.name("signon")).click();
    }
    @Then("je suis redirigé vers la page d'acceuil")
    public void je_suis_redirigé_vers_la_page_d_acceuil() {
        WebElement msg = driver.findElement(By.id("WelcomeContent"));
        System.out.println("message d'acceuil" + msg.getText());
        Assert.assertEquals("Welcome ABC!", msg.getText());
        driver.close();
    }
}