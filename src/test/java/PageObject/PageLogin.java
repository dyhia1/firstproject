package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageLogin {


    @FindBy(xpath = "//input[@name=\"username\"]")
    WebElement username;

    public WebElement getusername(){
        return username;
    }

    @FindBy(xpath = "//input[@name=\"password\"]")
    WebElement password;

    public WebElement getpassword(){
        return password;
    }

    @FindBy(xpath = "//input[@name=\"signon\"]")
    WebElement login;

    public WebElement getLogin(){
        return login;
    }




}
