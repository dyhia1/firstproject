package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageAcceuil {

    @FindBy (xpath = "//div[@id=\"MenuContent\"]/a[2]")
    WebElement signin;

    public WebElement goSignin(){
        return signin;
    }
}
