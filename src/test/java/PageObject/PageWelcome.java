package PageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageWelcome {

    @FindBy(xpath = "//div[@id=\"WelcomeContent\"]")
    WebElement msg;

    public WebElement getmsg(){
        return msg;
    }
}
