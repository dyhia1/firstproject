# language: en
Feature: connecter

	Scenario Outline: connecter
		Given je suis sur la page login
		When je saisis mon identifiant <username>
		And je saisis mon mot de passe <mdp>
		And je clique sur Login
		Then je suis redirigé vers la page d'acceuil

		@JDD1
		Examples:
		| mdp | username |
		| "j2ee" | "j2ee" |